Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 2005-2020, Mark A Lindner
License: LGPL-2.1+

Files: INSTALL
Copyright: 1994-1996, 1999-2002, 2004, Free
License: FSFUL

Files: Makefile.am
Copyright: debian/docs
License: LGPL-2.1+

Files: Makefile.in
 aclocal.m4
Copyright: 1994-2014, Free Software Foundation, Inc.
License: FSFULLR

Files: aux-build/*
Copyright: 1996-2013, Free Software Foundation, Inc.
License: GPL-2+ with Autoconf-data exception

Files: aux-build/config.rpath
Copyright: 1994-2014, Free Software Foundation, Inc.
License: FSFULLR

Files: aux-build/install-sh
Copyright: 1994, X Consortium
License: X11

Files: aux-build/ltmain.sh
Copyright: 1996-2001, 2003-2011, Free Software Foundation, Inc.
License: GPL-2+ with Libtool exception

Files: aux-build/texinfo.tex
Copyright: 1985, 1986, 1988, 1990-2013, Free Software Foundation, Inc.
License: GPL-3+

Files: configure
Copyright: 1992-1996, 1998-2012, Free Software Foundation, Inc.
License: FSFUL

Files: contrib/chained/examples/example1.cpp
 contrib/chained/examples/example2.cpp
 contrib/chained/examples/example3.cpp
 contrib/chained/examples/examples.cpp
Copyright: 2016, Richard Schubert
License: LGPL-2.1+

Files: contrib/chained/libconfig_chained.h
Copyright: 2016, Richard Schubert
License: LGPL-2.1+

Files: contrib/cmake/cmake_work/cmake_uninstall.cmake.in
Copyright: 2006-2008, The FLWOR Foundation.
License: Apache-2.0

Files: contrib/ls-config/debian/ls-config/usr/share/doc/ls-config/*
Copyright: 2013, Łukasz A. Grabowski <www@lucas.net.pl>
License: GPL-2+

Files: debian/*
Copyright: 2025, Mohammed Bilal <rmb@debian.org>
 2011-2013, Jonathan McCrohan <jmccrohan@gmail.com>
 2006-2009, Jose Luis Tallon <jltallon@adv-solutions.net>
License: GPL-2+

Files: doc/LGPL.texi
Copyright: 1991, 1999 Free Software Foundation, Inc.
License: LGPL-2.1

Files: doc/Makefile.in
Copyright: 1994-2014, Free Software Foundation, Inc.
License: FSFULLR

Files: doc/libconfig.texi
Copyright: 2004-2021, Mark A Lindner
License: Latex2e

Files: examples/Makefile.in
Copyright: 1994-2014, Free Software Foundation, Inc.
License: FSFULLR

Files: examples/c++/Makefile.in
Copyright: 1994-2014, Free Software Foundation, Inc.
License: FSFULLR

Files: examples/c/Makefile.in
Copyright: 1994-2014, Free Software Foundation, Inc.
License: FSFULLR

Files: lib/Makefile.in
Copyright: 1994-2014, Free Software Foundation, Inc.
License: FSFULLR

Files: lib/grammar.c
 lib/grammar.h
Copyright: 1984, 1989, 1990, 2000-2015, Free Software Foundation, Inc.
License: GPL-3+ with Bison-2.2 exception

Files: libconfig.spec
 libconfig.spec.in
Copyright: no-info-found
License: LGPL

Files: m4/*
Copyright: 1994-2014, Free Software Foundation, Inc.
License: FSFULLR

Files: m4/libtool.m4
Copyright: 1996-2001, 2003-2011, Free Software
License: (FSFULLR or GPL-2+) with Libtool exception

Files: m4/ltoptions.m4
Copyright: 2004, 2005, 2007-2009, Free Software Foundation
License: FSFULLR

Files: tests/Makefile.in
Copyright: 1994-2014, Free Software Foundation, Inc.
License: FSFULLR

Files: tinytest/Makefile.in
Copyright: 1994-2014, Free Software Foundation, Inc.
License: FSFULLR

Files: contrib/ls-config/lslib-core contrib/ls-config/sample/script
Copyright: 2013, Łukasz A. Grabowski <www@lucas.net.pl>
License: GPL-2+
